# This file contains metadata for your plugin.

# This file should be included when you package your plugin.# Mandatory items:

[general]
name=Inyouzu Maker
qgisMinimumVersion=3.0
description=Makes IN-YOU-ZU from DEM.
version=0.4.7
author=Aero Asahi Corp.
email=qgis.and.a@gmail.com

about=IN-YOU-ZU (陰陽図) is visualization map of Micro-landform. It shows terrain ruggedness with IN and YOU (dark-bright, 'yin and yang'). This plugin generates IN-YOU-ZU from Digital Elevation Model (DEM). This plugin only works on Windows.

tracker=https://bitbucket.org/aeroasahi/inyouzumaker/issues
repository=https://bitbucket.org/aeroasahi/inyouzumaker
# End of mandatory metadata

# Recommended items:

hasProcessingProvider=yes
# Uncomment the following line and add your changelog:
# changelog=

# Tags are comma separated with spaces allowed
tags=terrain, dem, dsm, dtm, raster

homepage=https://bitbucket.org/aeroasahi/inyouzumaker
category=Analysis
icon=icon.png
# experimental flag
experimental=False

# deprecated flag (applies to the whole plugin, not just a single version)
deprecated=False

# Since QGIS 3.8, a comma separated list of plugins to be installed
# (or upgraded) can be specified.
# Check the documentation for more information.
# plugin_dependencies=

Category of the plugin: Raster, Vector, Database or Web
# category=Raster

# If the plugin can run on QGIS Server.
server=False
